class Grid{

    constructor(rows, columns){
        this.rows = rows;
        this.columns = columns;
        this.initialize();
        this.configure_cells();
    }

    initialize(){
        this.grid=[];
        for(var i =0;i<this.rows;++i){
            var row=[];
            for(var j = 0; j<this.columns;j++){
                row[j]=new Cell(i, j);
            }

            this.grid[i]=row;
        }
    }

    configure_cells(){
        for(var i  = 0; i<this.rows;++i){
            for(var j  = 0; j<this.columns;++j){
               let cell =  this.grid[i][j];
               let row = cell.row;
               let col = cell.column;
               if(row>0) cell.north = this.grid[row-1][col];
               if(row<this.rows-1) cell.south = this.grid[row+1][col];
               if(col>0) cell.west = this.grid[row][col-1];
               if(col<this.columns-1) cell.east = this.grid[row][col+1];
            }
        }
    }

    *each_row(){
        for(let x of this.grid){
            yield x;
        }
    }

   * [Symbol.iterator]() {
        for(let i  of this.each_row()){
          for(let j  of i){
            yield j
          }
        }
   }

    toString(){
        let new_line = "<br/>";
        //let new_line = "\n";
        let output = "+" + "---+".repeat(this.columns)+ new_line;
        for(let row of this.each_row()){
            let top ="|";
            let bottom ="+";
            for(let cell of row){
                 if(!cell) cell = new Cell(-1,-1);
                 let body = "   ";
                 let east_boundary = (cell.linked(cell.east)?" ":"|");
                 top = top+body+ east_boundary;
                 let south_boundary = (cell.linked(cell.south)?"   ":"---");
                 let corner = "+";
                 bottom = bottom+south_boundary+corner;
            }
            output = output+top+new_line;
            output = output+bottom+new_line;
        }
        return output;
    }
}