class BinaryTree{

    constructor(grid){
        this.grid = grid
    }

    generate(){
        for(let actCell of this.grid){
            var nb = [];
            if(actCell.north) nb.push(actCell.north);
            if(actCell.east) nb.push(actCell.east);
            var item = nb[Math.floor(Math.random()*nb.length)];
            if(item) actCell.link(item);
        }
        return this.grid;

    }
}