class Cell {

    constructor(row, column){
        this.row = row;
        this.column=column;
        this.links = new Set();
    }

    link(cell, bidirectional=true){
        this.links.add(cell);
        if(bidirectional){
            cell.link(this, false);
        }
    }
    unlink(cell, bidirectional = true){
        this.links.delete(cell);
        if(bidirectional){
            cell.unlink(this, false);
        }
    }

    linked(cell){
//        if(cell===null) return false;
        return this.links.has(cell);
    }

    neighbors(){
        var neighbors = [];
        if(this.north){
            neighbors.push(this.north);
        }
        if(this.south){
            neighbors.push(this.south);
        }
        if(this.east){
            neighbors.push(this.east);
        }
        if(this.west){
            neighbors.push(this.west);
        }
        return neighbors;
    }
}