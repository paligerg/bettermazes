class Painter{
    constructor(grid, canvas){
    this.canvas=canvas;
    this.grid = grid;
    }

    paint(){
        let size = 20;
        this.canvas.width  = this.grid.columns*size+10;
        this.canvas.height = this.grid.rows*size+10;
        let ctx = this.canvas.getContext("2d");
        ctx.beginPath()

        for(let cell of this.grid){
            let y1 = 5+cell.row*size;
            let x1 = 5+cell.column*size;
            let y2 = 5+(cell.row+1)*size;
            let x2 = 5+(cell.column+1)*size

            ctx.moveTo(x1,y1);

            if(!cell.linked(cell.north)) {
                ctx.lineTo(x2, y1);
            } else {
                ctx.moveTo(x2, y1);
            }
            if(!cell.linked(cell.east)) {
                ctx.lineTo(x2,y2);
            } else {
                ctx.moveTo(x2, y2);
            }
            if(!cell.linked(cell.south)){
                ctx.lineTo(x1,y2)
            } else {
                ctx.moveTo(x1,y2)
            }
            if(!cell.linked(cell.west)){
                ctx.lineTo(x1,y1)
            } else {
                ctx.moveTo(x1,y1)
            }


        }
ctx.stroke();
    }
}